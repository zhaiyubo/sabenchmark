#ifndef BWA_H_
#define BWA_H_

#include <stdint.h>
#include <stdlib.h>
#include "bwt.h"

extern int bwa_verbose;

#ifdef __cplusplus
extern "C" {
#endif

	char *bwa_idx_infer_prefix(const char *hint);
	bwt_t *bwa_idx_load_bwt(const char *hint);

#ifdef __cplusplus
}
#endif

#endif
