#CFLAGS = -std=gnu99 -O3 -DPREALLOCATE=1 

all:sa occ sa_riscv occ_riscv

sa : *.c *.h
	gcc -DSA -o sa *.c -g 
sa_mt: *.c *.h
	gcc -DSA -DOPENMP -o sa_mt *.c -fopenmp -g
occ: *.c *.h
	gcc -DOCC -o occ *.c  -g
occ_mt: *.c *.h
	gcc -DOCC -DOPENMP -o occ_mt *.c -fopenmp -g
occ_gperf: *.c *.h
	gcc -DOCC -o occ_gperf *.c -lprofiler -g
#occ_prefetch : *.c *.h
#	gcc -DOCC -DDO_PREFETCH -o occ_prefetch *.c  -g
sa_riscv : *.c *.h
	riscv64-unknown-elf-gcc -DSA *.c -o sa_riscv.pk
occ_riscv : *.c *.h
	riscv64-unknown-elf-gcc -DOCC *.c -o occ_riscv.pk

clean :
	rm -rf sa occ *.pk
