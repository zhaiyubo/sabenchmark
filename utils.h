/* The MIT License

   Copyright (c) 2008 Genome Research Ltd (GRL).

   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
   BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
   ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

/* Contact: Heng Li <lh3@sanger.ac.uk> */

#ifndef LH3_UTILS_H
#define LH3_UTILS_H

#include <stdint.h>
#include <stdio.h>
//#include <zlib.h>

#ifdef __GNUC__
// Tell GCC to validate printf format string and args
#define ATTRIBUTE(list) __attribute__ (list)
#else
#define ATTRIBUTE(list)
#endif

#define xopen(fn, mode) err_xopen_core(__func__, fn, mode)
#define xassert(cond, msg) if ((cond) == 0) _err_fatal_simple_core(__func__, msg)

#ifdef __cplusplus
extern "C" {
#endif

	void err_fatal(const char *header, const char *fmt, ...) ATTRIBUTE((noreturn));
	void err_fatal_core(const char *header, const char *fmt, ...) ATTRIBUTE((noreturn));
	void _err_fatal_simple(const char *func, const char *msg) ATTRIBUTE((noreturn));
	void _err_fatal_simple_core(const char *func, const char *msg) ATTRIBUTE((noreturn));
	FILE *err_xopen_core(const char *func, const char *fn, const char *mode);  
	size_t err_fread_noeof(void *ptr, size_t size, size_t nmemb, FILE *stream);
	int err_fseek(FILE *stream, long offset, int whence);
	long err_ftell(FILE *stream);
	int err_fclose(FILE *stream);

#ifdef __cplusplus
}
#endif

static inline uint64_t hash_64(uint64_t key)
{
	key += ~(key << 32);
	key ^= (key >> 22);
	key += ~(key << 13);
	key ^= (key >> 8);
	key += (key << 3);
	key ^= (key >> 15);
	key += ~(key << 27);
	key ^= (key >> 31);
	return key;
}

#endif

#define RDTSC_S(val) do {                                           \
    uint64_t __a,__d;                                             \
    asm volatile(/*"cpuid\n\t"*/                                     \
            "rdtsc\n\t"                                        \
            "mov %%rdx, %0\n\t"                                        \
            "mov %%rax, %1\n\t"                                        \
            :"=r" (__d), "=r" (__a)::"%rax","%rbx","%rcx","%rdx");         \
    (val) = ((uint64_t)__a) | (((uint64_t)__d)<<32);     \
  } while(0)

static inline uint64_t rdtsc_s() {
  uint64_t val;
  RDTSC_S(val);
  return val;
}

#define RDTSC_E(val_e) do {                                           \
    uint64_t __a_e,__d_e;                                             \
    asm volatile("rdtscp\n\t"                                        \
            "mov %%rdx, %0\n\t"                                        \
            "mov %%rax, %1\n\t"                                        \
            /*"cpuid\n\t"*/                                        \
            :"=r" (__d_e), "=r" (__a_e)::"%rax","%rbx","%rcx","%rdx");         \
    (val_e) = ((uint64_t)__a_e) | (((uint64_t)__d_e)<<32);     \
  } while(0)

static inline uint64_t rdtsc_e() {
  uint64_t val_e;
  RDTSC_E(val_e);
  return val_e;
}
