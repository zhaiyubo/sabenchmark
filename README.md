# Simplified Code version 1.0 （Multi-Threading Version）
- Remove bwt_2occ4 and all callings go to bwt_occ4
- No performance loss
- Remove input files
- Extract fscanf and remove memcpy
- Use CPUID + RDTSC
# Multithreading GCC
- export GOMP_CPU_AFFINITY="0-15:1" 
- export OMP_NUM_THREADS=16
# Multithreading ICC
- export KMP_PLACE_THREADS=1T
- export KMP_AFFINITY=compact,granularity=fine
- export OMP_NUM_THREADS=
# Bound number
- In main.c: #define Max_Bound_Num =

```
make occ
./occ ~/sample/Saccharomyces_cerevisiae.SGD1.01.50.dna_rm.toplevel.fa
```

