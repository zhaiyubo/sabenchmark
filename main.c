#include <stdio.h>
#include <string.h>
#include <time.h>
#include "bwt.h"
#include "bwa.h"
#include "utils.h"

//#include <gperftools/profiler.h>
#include <omp.h>
#define OPENMP
#define MAX_PREFIX_LEN 100
#define MAX_BOUND_NUM 100000000
//#define MAX_BOUND_NUM 100

FILE *sa_in;
FILE *sa_out;
FILE *occ_in;
FILE *occ_out;

#define T_SIZE 10
uint64_t ts[T_SIZE];
uint64_t te[T_SIZE];
uint64_t delt[T_SIZE];

// return the difference in second between two timeval structures
long diff_in_us(struct timeval *finishtime, struct timeval * starttime)
{
	long sec;
	sec=(finishtime->tv_sec - starttime->tv_sec)*1000000;
	sec+=finishtime->tv_usec - starttime->tv_usec;
	return sec;
}

int main(int argc, char *argv[])
{
    int i, j;
    struct timeval start,end;
    char *hint;
    bwt_t *bwt;
    int numthreads = 1;
    hint = calloc(MAX_PREFIX_LEN, 1);
    hint = argv[1];
    numthreads = atoi(argv[2]);
    
    bwt = bwa_idx_load_bwt(hint);

    memset(ts, 0, T_SIZE*8);
    memset(te, 0, T_SIZE*8);
    memset(delt, 0, T_SIZE*8);
    #ifdef OCC
    bwtint_t tl[4];
    bwtint_t *bound = (bwtint_t*)malloc(sizeof(bwtint_t)*MAX_BOUND_NUM);
    //printf("123\n");
    occ_in = fopen("./input/bwt_occ_in","r");
    //occ_out = fopen("./output/cpu_occ_out","w+");
    //printf("123\n");
    for(i=0;i<MAX_BOUND_NUM;i++)
        fscanf(occ_in, "%lld", bound+i);
    gettimeofday(&start,NULL);
    //printf("123\n");
    #ifdef OPENMP
    //double start_omp = omp_get_wtime();
    //#pragma omp parallel num_threads(THREAD_NUM) private(tl) shared(occ_out,bound,j,bwt) 
    #pragma omp parallel num_threads(numthreads) private(tl) shared(occ_out,bound,j,bwt) 
    {
    #pragma omp for 
    #endif
   	for(j=0;j<MAX_BOUND_NUM;j++)
	{
		//printf("123\n");
		bwt_occ4(bwt, *(bound+j), tl);
    		//fprintf(occ_out, "%lld   %lld    %lld    %lld\n", tl[0], tl[1], tl[2], tl[3]);
	}
    #ifdef OPENMP
    }
    //double end_omp = omp_get_wtime();
    #endif
    //printf("123\n");
    gettimeofday(&end,NULL);
    long occ4Time = (end.tv_sec-start.tv_sec)*1000000 + (end.tv_usec - start.tv_usec);
    fclose(occ_in);
    free(bound);
    printf("occ4Time is:%ld\n",occ4Time);
    #endif

    #ifdef SA
	bwtint_t coordinate, index;

    sa_in = fopen("./input/bwt_sa_in", "r");
    //sa_out = fopen("./output/cpu_sa_out","w+");
        
    //unsigned long time_used = 0;
    //

    bwtint_t *sa_bound = (bwtint_t*)malloc(sizeof(bwtint_t)*MAX_BOUND_NUM);
    for(i=0;i<MAX_BOUND_NUM;i++)
        fscanf(sa_in, "%lld", sa_bound+i);
    
	//while(fscanf(sa_in, "%lld", &index) == 1)
    #ifdef OPENMP
    double start_omp_sa = omp_get_wtime();
    //#pragma omp parallel num_threads(THREAD_NUM) private(tl) shared(occ_out,bound,j,bwt) 
    #pragma omp parallel shared(sa_out,sa_bound,j,bwt) 
    {
    #pragma omp for
    #endif
   	for(j=0;j<MAX_BOUND_NUM;j++)
	{
        //gettimeofday(&start, NULL);
		
        //coordinate = bwt_sa(bwt, sa_bound[j]);
        bwt_occ(bwt,sa_bound[j],0);
        
        //gettimeofday(&finish, NULL);
        //time_used += diff_in_us(&finish, &start);
		//fprintf(sa_out, "%lld\n", coordinate);
	}
    #ifdef OPENMP
    }
    double end_omp_sa = omp_get_wtime();
    #endif

    //fprintf(stderr, "\n***CPU version time used %ld seconds!***\n\n", time_used);
    
    //For small files, we view the difference of sa_out from bwt_sa_out  with the "diff" command
    //printf("bwt_sa parallel time: %f sec\n",end_omp_sa-start_omp_sa);
    
    fclose(sa_in);
    free(sa_bound);
    #endif
	
    bwt_destroy(bwt);    
    
    return 0;
}

